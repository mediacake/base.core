<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadLanguageFile(__FILE__);

class base_core extends CModule
{
	const ON_MODULE_INCLUDE = 'OnModuleInclude';

	public $MODULE_ID = 'base.core';
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_GROUP_RIGHTS = 'N';

	public function __construct()
	{
		$arModuleVersion = array();
		include __DIR__ . '/version.php';
		
		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		$this->MODULE_NAME = Loc::getMessage('base.core_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('base.core_MODULE_DESCRIPTION');
		$this->PARTNER_NAME = Loc::getMessage('base.core_PARTNER_NAME');
		$this->PARTNER_URI = Loc::getMessage('base.core_PARTNER_URI');
	}

	function DoInstall()
	{
		ModuleManager::registerModule($this->MODULE_ID);
	}

	function DoUninstall()
	{
		ModuleManager::unRegisterModule($this->MODULE_ID);
	}
}