<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var BaseCoreSampleAlphabetComponent|\Base\Core\Component\Base|CBitrixComponent $component */
$this->setFrameMode(true); ?>

<? if($arResult['ITEMS']): ?>
	<ul>
		<? foreach($arResult['ITEMS'] as $item): ?>
			<li><?=$item?></li>
		<? endforeach; ?>
	</ul>
<? endif ?>

