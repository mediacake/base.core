<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

use Base\Core\Component\Parameters;

try
{
	$arComponentParameters = Parameters::init();
}
catch(\Exception $e)
{
	ShowError($e->getMessage());
}