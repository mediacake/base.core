<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Base\Core\Component\Base;
use Bitrix\Main\Application;

class BaseCoreSampleAlphabetComponent extends Base
{
	protected function processComponentResult(array $params)
	{
		$result = array();

		$helper = Application::getConnection()->getSqlHelper();

		$iterator = \Bitrix\Iblock\ElementTable::query()
			->registerRuntimeField(
				'LETTER',
				array(
					'data_type' => 'string',
					'expression' => array($helper->getSubstrFunction('TRIM(NAME)', 1, 1))
				)
			)
			->setSelect(array('LETTER'))
			->setGroup(array('LETTER'))
			->setOrder(array('LETTER' => 'ASC'))
			->exec();

		$result['ITEMS'] = array();

		while($item = $iterator->fetch())
		{
			$result['ITEMS'][] = $item['LETTER'];
		}

		return $result;
	}
}