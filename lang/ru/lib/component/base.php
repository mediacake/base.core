<?php
$MESS['CORE_COMPONENT_ERROR_REQUIRED_PARAMETER'] = 'Не указан обязательный параметр #PARAM#.';
$MESS['CORE_COMPONENT_ERROR_MODULE_NOT_INSTALLED'] = 'Модуль "#MODULE#" не установлен.';
$MESS['CORE_COMPONENT_ERROR_NOT_FOUND_OBJECT'] = 'Обьект не найден';
