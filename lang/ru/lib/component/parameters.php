<?php
$MESS['CORE_COMPONENT_PARAMETERS_FILTER_NAME'] = 'Имя выходящего массива для фильтрации';
$MESS['CORE_COMPONENT_PARAMETERS_SAVE_IN_SESSION'] = 'Сохранять установки фильтра в сессии пользователя';
$MESS['CORE_COMPONENT_PARAMETERS_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['CORE_COMPONENT_PARAMETERS_IBLOCK_ID'] = 'Инфоблок';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_ASC'] = 'по возрастанию';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_DESC'] = 'по убыванию';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_BY1'] = 'По какому полю сортируем';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_ORDER1'] = 'Порядок сортировки';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_BY2'] = 'Поле для второй сортировки';
$MESS['CORE_COMPONENT_PARAMETERS_SORT_ORDER2'] = 'Порядок второй сортировки';
$MESS['CORE_COMPONENT_PARAMETERS_CHECK_DATES'] = 'Показывать только активные на данный момент';
$MESS['CORE_COMPONENT_PARAMETERS_PAGE_ELEMENT_COUNT'] = 'Количество на странице';
$MESS["CORE_COMPONENT_PARAMETERS_SECTION_ID"] = "ID раздела";
$MESS["CORE_COMPONENT_PARAMETERS_SECTION_CODE"] = "Символьный код раздела";
$MESS['CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS'] = 'Показывать элементы подразделов раздела';
$MESS['CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_ALL'] = 'всех подразделов';
$MESS['CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_ACTIVE'] = 'активных подразделов';
$MESS['CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_NO'] = 'не показывать';
$MESS['CORE_COMPONENT_PARAMETERS_CHECK_DATES'] = 'Показывать только активные на данный момент';
$MESS['CORE_COMPONENT_PARAMETERS_PAGE_ELEMENT_COUNT'] = 'Количество на странице';
$MESS['CORE_COMPONENT_PARAMETERS_ACTIVE_DATE_FORMAT'] = 'Формат показа даты';
$MESS['CORE_COMPONENT_PARAMETERS_IBLOCK_FIELD'] = 'Поля';
$MESS['CORE_COMPONENT_PARAMETERS_CACHE_TEMPLATE'] = 'Кешировать шаблон';
$MESS['CORE_COMPONENT_PARAMETERS_ID'] = 'ID элемента';
$MESS['CORE_COMPONENT_PARAMETERS_ID_TYPE'] = 'Тип элемента';
$MESS['CORE_COMPONENT_PARAMETERS_ELEMENT_ID'] = 'ID элемента';
$MESS['CORE_COMPONENT_PARAMETERS_ELEMENT_CODE'] = 'Символьный код элемента';
$MESS['CORE_COMPONENT_PARAMETERS_COLOR'] = 'Цвет';
$MESS['CORE_COMPONENT_PARAMETERS_USER_ID'] = 'Пользователь';
$MESS['CORE_COMPONENT_PARAMETERS_PRICE_CODE'] = 'Тип цены';
