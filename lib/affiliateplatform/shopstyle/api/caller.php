<?php

namespace Base\Core\AffiliatePlatform\Shopstyle\Api;

use Base\Core\AffiliatePlatform\ShopStyle;
use Base\Core\Util\Logger;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Text\BinaryString;
use Bitrix\Main\Text\Encoding;
use Bitrix\Main\Web\HttpClient;

class Caller
{
	protected $http;
	protected $apiUrl;
	protected $apiKey;
	protected $siteId;

	public function __construct($params)
	{
		$this->http = new HttpClient(array(
			'version' => '1.1',
			'socketTimeout' => 60,
			'streamTimeout' => 60,
			'redirect' => true,
			'redirectMax' => 5,
		));

		if(!isset($params['URL']))
		{
			throw new ArgumentNullException('params["URL"]');
		}

		if(!isset($params['API_KEY']))
		{
			throw new ArgumentNullException('params["API_KEY"]');
		}

		if(!isset($params['SITE_ID']))
		{
			throw new ArgumentNullException('params["SITE_ID"]');
		}

		$this->apiKey = $params['API_KEY'];
		$this->apiUrl = $params['URL'];
		$this->siteId = $params['SITE_ID'];
	}

	public function sendRequest($callName, $data)
	{
		if(strlen($callName) <= 0)
		{
			throw new ArgumentNullException('callName');
		}

		if(BinaryString::changeCaseToLower(SITE_CHARSET) != 'utf-8')
		{
			$data = Encoding::convertEncoding($data, SITE_CHARSET, 'UTF-8');
		}

		$result = @$this->http->post($this->apiUrl, $data);
		$errors = $this->http->getError();

		if(!$result && !empty($errors))
		{
			$strError = '';

			foreach($errors as $errorCode => $errMes)
			{
				$strError .= $errorCode . ': ' . $errMes;
			}

			ShopStyle::log(Logger::LOG_LEVEL_INFO, 'SHOPSTYLE_REQUEST_ERROR', $callName, $strError, $this->siteId);
		}
		else
		{
			$status = $this->http->getStatus();

			if($status != 200)
			{
				ShopStyle::log(Logger::LOG_LEVEL_INFO, 'SHOPSTYLE_REQUEST_HTTP_ERROR', $callName, 'HTTP error code: ' . $status, $this->siteId);
			}

			if(strtolower(SITE_CHARSET) != 'utf-8')
			{
				$result = Encoding::convertEncoding($result, 'UTF-8', SITE_CHARSET);
			}
		}

		return $result;
	}
} 