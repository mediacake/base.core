<?php

namespace Base\Core\AffiliatePlatform;

use Base\Core\Util\Logger;
use Bitrix\Main\SystemException;

/**
 * Class ShopStyle
 *
 * @package Base\Core\AffiliatePlatform
 */
class ShopStyle extends Platform
{
	/**
	 * @var ShopStyle
	 */
	protected static $instance = null;

	/**
	 * Returns current instance of the Application.
	 *
	 * @return ShopStyle
	 * @throws SystemException
	 */
	public static function getInstance()
	{
		if(!isset(static::$instance))
		{
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Log events to system log & sends error to email.
	 *
	 * @param int $level Log level of event.
	 * @param string $type Event type.
	 * @param string $itemId Item id.
	 * @param string $description Event description.
	 * @param string $siteId Site id.
	 *
	 * @return bool
	 */
	public static function log($level, $type, $itemId, $description, $siteId)
	{
		static $shopstyle = null;

		if($shopstyle === null)
		{
			$shopstyle = self::getInstance();
			$settings = $shopstyle->getSettings();

			if(isset($settings[$siteId]['LOG_LEVEL']))
			{
				$logLevel = $settings[$siteId]['LOG_LEVEL'];
				$shopstyle->logger->setLevel($logLevel);
			}
		}

		if($level == Logger::LOG_LEVEL_ERROR)
		{
			$shopstyle->sendErrorMail($type, $description, $siteId);
		}

		return $shopstyle->addLogRecord($level, $type, $itemId, $description);
	}


	/**
	 * Sends error description to e-mail
	 *
	 * @param string $type Type of error.
	 * @param string $details Error details.
	 * @param string $siteId Site id.
	 *
	 * @return bool
	 */
	public function sendErrorMail($type, $details, $siteId)
	{
		if(!isset($this->settings[$siteId]['EMAIL_ERRORS']) || strlen($this->settings[$siteId]['EMAIL_ERRORS']) <= 0)
		{
			return false;
		}

		$loggerTypes = Helper::OnEventLogGetAuditTypes();
		$errorType = isset($loggerTypes[$type]) ? $loggerTypes[$type] : $type;

		$fields = array(
			'EMAIL_TO' => $this->settings[$siteId]['EMAIL_ERRORS'],
			'ERROR_TYPE' => $errorType,
			'ERROR_DETAILS' => $details
		);

		$event = new \CEvent;

		return $event->Send('SALE_EBAY_ERROR', $siteId, $fields, 'N');
	}

	/**
	 * @param int $level The level of event.
	 * @param string $type Type of event.
	 * @param string $itemId Item idenifyer.
	 * @param string $description Description of event.
	 *
	 * @return bool Success or not.
	 */
	public function addLogRecord($level, $type, $itemId, $description)
	{
		return $this->logger->addRecord($level, $type, $itemId, $description);
	}
}