<?php

namespace Base\Core\AffiliatePlatform;

use Base\Core\Util\Logger;

/**
 * Class Platform
 *
 * @package Base\Core\AffiliatePlatform
 */
abstract class Platform
{
	protected $logger;
	protected $logLevel = Logger::LOG_LEVEL_ERROR;

	/**
	 * Platform constructor.
	 */
	protected function __construct()
	{
		$this->logger = new Logger($this->logLevel);
	}

	protected function __clone(){}
}