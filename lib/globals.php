<?php

namespace Base\Core;

use Bitrix\Main\Application;

/**
 * Class Globals
 *
 * @package Base\Core
 */
trait Globals
{
	/**
	 * Gets current user.
	 *
	 * @return array|bool|\CAllUser|\CUser
	 */
	protected function getUser()
	{
		global $USER;

		return $USER;
	}

	/**
	 * @return \CUserTypeManager
	 */
	protected function getUserFieldManager()
	{
		global $USER_FIELD_MANAGER;

		return $USER_FIELD_MANAGER;
	}

	/**
	 * Get application instance.
	 *
	 * @return Application|\Bitrix\Main\HttpApplication|\CAllMain|\CMain
	 */
	protected function getApplication()
	{
		global $APPLICATION;

		return $APPLICATION;
	}
}