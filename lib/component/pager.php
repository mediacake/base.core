<?php

namespace Base\Core\Component;

use CDBResult;
use CIBlockResult;

trait Pager
{
	/** @var array Navigation parameters */
	protected $navParams;
	protected $navigation;

	/**
	 * Parse navigation params
	 *
	 * @param array $params
	 * @param string $prefix
	 *
	 * @return array
	 */
	protected function parsePagerParams(array &$params, $prefix = '')
	{
		if(array_key_exists($prefix . 'PAGE_ELEMENT_COUNT', $params) && Tools::parseInt($params[$prefix . 'PAGE_ELEMENT_COUNT']) > 0)
		{
			if($params[$prefix . 'DISPLAY_TOP_PAGER'] || $params[$prefix . 'DISPLAY_BOTTOM_PAGER'])
			{
				Tools::parseString($params[$prefix . 'PAGER_TITLE']);
				Tools::parseString($params[$prefix . 'PAGER_TEMPLATE']);
				Tools::parseBoolean($params[$prefix . 'DISPLAY_TOP_PAGER']);
				Tools::parseBoolean($params[$prefix . 'DISPLAY_BOTTOM_PAGER']);
				Tools::parseBoolean($params[$prefix . 'PAGER_SHOW_ALWAYS']);
				Tools::parseBoolean($params[$prefix . 'PAGER_SHOW_ALL']);

				$this->navParams[$prefix] = array(
					'nPageSize' => $params[$prefix . 'PAGE_ELEMENT_COUNT'],
					'bShowAll' => $params[$prefix . 'PAGER_SHOW_ALL'],
				);

				$this->navigation[$prefix] = CDBResult::GetNavParams($this->navParams);
			}
			else if($params[$prefix . 'PAGE_ELEMENT_COUNT'])
			{
				$this->navParams[$prefix] = array(
					'nTopCount' => $params[$prefix . 'PAGE_ELEMENT_COUNT'],
				);
				$this->navigation = false;
			}
			else
			{
				$this->navParams = false;
				$this->navigation = false;
			}
		}

		return $params;
	}

	/**
	 * @return array
	 */
	public function getNavParams($prefix = '')
	{
		return $this->navParams[$prefix];
	}

	/**
	 * @return mixed
	 */
	public function getNavigation()
	{
		return $this->navigation;
	}

	/**
	 * @param CIBlockResult $iterator
	 * @param array $params
	 * @param string $prefix
	 *
	 * @return mixed
	 */
	public function getNavString(CIBlockResult $iterator, array $params, $prefix = '')
	{
		$string = false;

		if($params[$prefix . 'DISPLAY_TOP_PAGER'] || $params[$prefix . 'DISPLAY_BOTTOM_PAGER'])
		{
			$string = trim($iterator->GetPageNavStringEx($navComponentObject, $params[$prefix . 'PAGER_TITLE'], $params[$prefix . 'PAGER_TEMPLATE'], $params[$prefix . 'PAGER_SHOW_ALWAYS']));
		}

		return $string;
	}
}