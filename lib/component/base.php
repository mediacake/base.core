<?php

namespace Base\Core\Component;

use Base\Core\Globals;
use Bitrix\Main\Application;
use Bitrix\Main\DB\SqlQueryException;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;
use CAjax;
use CBitrixComponent;
use CPageOption;
use Exception;

Loc::loadMessages(__FILE__);

/**
 * Abstract class for all components.
 *
 * @package Base\Core\Component
 */
abstract class Base extends CBitrixComponent
{
	use Controller, Modules, Pager, Globals;

	const ERROR_REQUIRED_PARAMETER = 'CORE_COMPONENT_22001';
	const ERROR_MODULE_NOT_INSTALLED = 'CORE_COMPONENT_22002';
	const ERROR_NOT_FOUND_OBJECT = 'CORE_COMPONENT_22003';

	const STATUS_SUCCESS = 'success';
	const STATUS_DENIED = 'denied';
	const STATUS_ERROR = 'error';
	const STATUS_NEED_AUTH = 'need_auth';
	const STATUS_INVALID_SIGN = 'invalid_sign';
	const STATUS_NOT_FOUND = 'not_found';

	const AJAX_PARAM_ID = 'bxajax';

	/** @var  string */
	protected $actionPrefix = 'action';
	/** @var  string */
	protected $action;
	/** @var  array */
	protected $actionDescription;
	/** @var  string */
	protected $realActionName;
	/** @var  ErrorCollection */
	protected $errorCollection;
	protected $componentId = '';
	/** @var array */
	protected $cacheAdditionalId = array();
	/**
	 * @var array|\Bitrix\Main\Data\TaggedCache
	 */
	protected $taggedCache = array();

	/** @inheritdoc */
	public function __construct($component = null)
	{
		parent::__construct($component);

		$this->errorCollection = new ErrorCollection();
		$this->setComponentId();
		$this->taggedCache = Application::getInstance()->getTaggedCache();

		CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
	}

	/** @inheritdoc */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}

	/** @inheritdoc */
	public function onPrepareComponentParams($params)
	{
		Parameters::parseDefaultSettings($params);

		return $params;
	}

	/** @inheritdoc */
	public function executeComponent()
	{
		try
		{
			$this->execActions();
			$this->start();

			if($this->startResultCache(false, array_merge($this->getCacheAdditionalId(), $this->getNavigation())))
			{
				$this->arResult = $this->processComponentResult($this->arParams);

				if($this->arParams['CACHE_TEMPLATE'])
				{
					$this->includeComponentTemplate();
				}
				$this->endResultCache();
			}
			else
			{
				$this->abortResultCache();
			}

			if(!$this->arParams['CACHE_TEMPLATE'])
			{
				$this->includeComponentTemplate();
			}

			$this->end();

			return $this->processAfterResult($this->arParams, $this->arResult);
		}
		catch(SqlQueryException $e)
		{
			pre($e->getDatabaseMessage());
			pre($e->getMessage());
			pre($e->getQuery());

			$this->runProcessingExceptionComponent($e);
		}
	}

	/**
	 * Executes controller by specific action.
	 * This method contains all steps of life cycle controller.
	 *
	 * @return void
	 */
	public function execActions()
	{
		$this->resolveAction();
		$this->checkAction();
		$this->checkRequiredModules();

		if($this->processBeforeAction($this->getAction()) !== false)
		{
			$this->runAction();
		}
	}

	public function getComponentId()
	{
		return $this->componentId;
	}

	/**
	 * @param string $componentId
	 */
	public function setComponentId($componentId = '')
	{
		if($componentId)
		{
			$this->componentId = md5($componentId);
		}
		else
		{
			$this->componentId = CAjax::GetComponentID($this->getName(), $this->getTemplateName(), $this->getCacheID());
		}
	}

	/** @inheritdoc */
	protected function resolveAction()
	{
		$listOfActions = $this->normalizeListOfAction($this->listActions());
		$this->realActionName = null;
		$action = $this->request->get($this->actionPrefix);

		if($action && is_string($action) && isset($listOfActions[strtolower($action)]))
		{
			$this->realActionName = strtolower($action);
			$description = $listOfActions[$this->realActionName];
			$this->setAction($description['name'], $description);

			return $this;
		}
	}

	/**
	 * @return bool
	 */
	protected function processAfterResult()
	{
		return true;
	}

	/**
	 * Sends JSON response with status "error" and with errors and terminates controller.
	 *
	 * @return void
	 */
	protected function sendJsonErrorResponse()
	{
		$errors = array();
		foreach($this->getErrors() as $error)
		{
			/** @var Error $error */
			$errors[] = array(
				'message' => $error->getMessage(),
				'code' => $error->getCode(),
			);
		}
		unset($error);
		$this->sendJsonResponse(array(
			'status' => self::STATUS_ERROR,
			'errors' => $errors,
		));
	}

	/**
	 * Sends JSON response with status "success" and mixed data, and terminates controller.
	 *
	 * @param array $response Data to response.
	 *
	 * @return void
	 */
	protected function sendJsonSuccessResponse(array $response = array())
	{
		$response['status'] = self::STATUS_SUCCESS;
		$this->sendJsonResponse($response);
	}

	protected function sendJsonResponse($response)
	{
		if(!defined('PUBLIC_AJAX_MODE'))
		{
			define('PUBLIC_AJAX_MODE', true);
		}

		$this->start();

		header('Content-Type:application/json; charset=UTF-8');
		echo Json::encode($response);

		$this->end();
	}

	/** @inheritdoc */
	protected function checkAction()
	{
		/*if($this->errorCollection->hasErrors())
		{
			$this->sendErrorResponse();
		}*/

		if($description = $this->getActionDescription())
		{

			if(!$this->getUser() || !$this->getUser()->getId())
			{
				if($description['redirect_on_auth'])
				{
					LocalRedirect(SITE_DIR . 'auth/?backurl=' . urlencode(Application::getInstance()->getContext()->getRequest()->getRequestUri()));
				}
				else
				{
					//$this->runProcessingIfUserNotAuthorized();
				}
			}

			//if does not exist check_csrf_token we have to check csrf for only POST method.
			if($description['check_csrf_token'] === true || ($this->request->isPost() && !isset($description['check_csrf_token'])))
			{
				if(!check_bitrix_sessid())
				{
					LocalRedirect(SITE_DIR . 'auth/?backurl=' . urlencode(Application::getInstance()->getContext()->getRequest()->getRequestUri()));
					//$this->sendAccessDeniedResponse('Wrong csrf token');
				}
			}

			if(!in_array($this->request->getRequestMethod(), $description['method']))
			{
				LocalRedirect(SITE_DIR . 'auth/?backurl=' . urlencode(Application::getInstance()->getContext()->getRequest()->getRequestUri()));
				//$this->sendAccessDeniedResponse('Wrong method for current action');
			}
		}
	}

	/** @inheritdoc */
	protected function sendErrorResponse()
	{
		pre($this->getErrors());
	}

	/**
	 * Getting array of errors.
	 *
	 * @return Error[]
	 */
	public function getErrors()
	{
		return $this->errorCollection->toArray();
	}

	/** @inheritdoc */
	protected function getActionDescription()
	{
		return $this->actionDescription;
	}

	/** @inheritdoc */
	protected function getAction()
	{
		return $this->action;
	}

	/** @inheritdoc */
	protected function setAction($action, array $description)
	{
		$this->action = $action;
		$this->actionDescription = $description;

		return $this;
	}

	/** @inheritdoc */
	protected function runAction()
	{
		$ajaxId = $this->request->get(self::AJAX_PARAM_ID);

		if($action = $this->getAction())
		{
			if(empty($ajaxId) || (!empty($ajaxId) && $ajaxId == $this->getComponentId()))
			{
				$actionMethod = 'processAction' . $action;

				return $this->$actionMethod();
			}
		}
	}

	/**
	 * Get additional ID to cache
	 *
	 * @return array
	 */
	final protected function getCacheAdditionalId()
	{
		return $this->cacheAdditionalId;
	}

	/**
	 * Add additional ID to cache
	 *
	 * @param mixed $id
	 */
	final protected function setCacheAdditionalId($id)
	{
		$this->cacheAdditionalId[] = $id;
	}

	/**
	 * @param array $params
	 *
	 * @return mixed
	 */
	protected function processComponentResult(array $params)
	{
		return array();
	}

	/**
	 * @param Exception $e
	 *
	 * @throws \Exception
	 */
	protected function runProcessingExceptionComponent(Exception $e)
	{
		$this->errorCollection->add(array(new Error($e->getMessage())));
		$this->sendErrorResponse();
	}

	/**
	 * Getting array of errors with the necessary code.
	 *
	 * @param string $code Code of error.
	 *
	 * @return Error[]
	 */
	public function getErrorsByCode($code)
	{
		return $this->errorCollection->getErrorsByCode($code);
	}

	/**
	 * Getting once error with the necessary code.
	 *
	 * @param string $code Code of error.
	 *
	 * @return Error[]
	 */
	public function getErrorByCode($code)
	{
		return $this->errorCollection->getErrorByCode($code);
	}

	/**
	 * Returns whether this is an AJAX (XMLHttpRequest) request.
	 *
	 * @return boolean
	 */
	protected function isAjaxRequest()
	{
		return ($this->request->get(self::AJAX_PARAM_ID) == $this->getComponentId())
		&& $this->request->isAjaxRequest();
	}

	/** @inheritdoc */
	protected function checkRequiredInputParams(array $inputParams, array $required)
	{
		foreach($required as $item)
		{
			if(!isset($inputParams[$item]) || (!$inputParams[$item] && !(is_string($inputParams[$item]) && strlen($inputParams[$item]))))
			{
				$this->errorCollection->add(array(new Error(Loc::getMessage('CORE_COMPONENT_ERROR_REQUIRED_PARAMETER', array('#PARAM#' => $item)), self::ERROR_REQUIRED_PARAMETER)));

				return false;
			}
		}

		return true;
	}

	protected function start()
	{
		if($this->isAjaxRequest())
		{
			$this->getApplication()->restartBuffer();
			while(ob_end_clean())
			{
				;
			}

		}
	}

	protected function end()
	{
		if($this->isAjaxRequest())
		{
			/** @noinspection PhpUndefinedClassInspection */
			\CMain::finalActions();
			die;
		}
	}
}