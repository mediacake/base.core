<?php

namespace Base\Core\Component;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use CIBlockParameters;

Loc::loadMessages(__FILE__);

class Parameters extends CIBlockParameters
{
	/**
	 * @return array
	 */
	public static function init()
	{
		return array(
			'GROUPS' => array(),
			'PARAMETERS' => array(
				'FILTER_NAME' => array(
					'PARENT' => 'DATA_SOURCE',
					'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_FILTER_NAME'),
					'TYPE' => 'STRING',
					'DEFAULT' => '',
				),
				'SAVE_IN_SESSION' => array(
					'PARENT' => 'DATA_SOURCE',
					'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SAVE_IN_SESSION'),
					'TYPE' => 'CHECKBOX',
					'DEFAULT' => 'N',
				),
				'CACHE_TEMPLATE' => array(
					'PARENT' => 'CACHE_SETTINGS',
					'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_CACHE_TEMPLATE'),
					'TYPE' => 'CHECKBOX',
					'DEFAULT' => 'Y',
				),
				'CACHE_TIME' => array('DEFAULT' => 36000000),
			),
		);
	}

	/**
	 * @param array $arComponentParameters
	 * @param array $rules
	 *
	 * @return array
	 */
	public static function router(array &$arComponentParameters, array $rules = array())
	{
		$arComponentParameters['PARAMETERS']['SEF_MODE'] = $rules;

		return $arComponentParameters;
	}

	/**
	 * @param array $arComponentParameters
	 *
	 * @return array
	 */
	public static function addTitle(array &$arComponentParameters)
	{
		$arComponentParameters['PARAMETERS']['SET_TITLE'] = array();

		return $arComponentParameters;
	}

	/**
	 * @param array $arComponentParameters
	 * @param $arCurrentValues
	 * @param string $prefix
	 * @param string $parent
	 * @param string $name
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function addIblockSettings(array &$arComponentParameters, $arCurrentValues, $prefix = '', $parent = 'BASE', $name = '')
	{
		if(!isset($arComponentParameters['GROUPS']))
		{
			$arComponentParameters['GROUPS'] = array();
		}

		if($name)
		{
			$arComponentParameters['GROUPS'][$parent] = array(
				'NAME' => $name,
			);
		}

		$types = CIBlockParameters::GetIBlockTypes();

		$arComponentParameters['PARAMETERS'][$prefix . 'IBLOCK_TYPE'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $types,
			'REFRESH' => 'Y',
		);
		if($types && empty($arCurrentValues[$prefix . 'IBLOCK_TYPE']) || !array_key_exists($arCurrentValues[$prefix . 'IBLOCK_TYPE'], $types))
		{
			$arCurrentValues[$prefix . 'IBLOCK_TYPE'] = current(array_keys($types));
		}
		if($arCurrentValues[$prefix . 'IBLOCK_TYPE'])
		{
			$iterator = IblockTable::getList(array(
				'order' => array('SORT' => 'asc', 'NAME' => 'asc'),
				'filter' => array('=IBLOCK_TYPE_ID' => $arCurrentValues[$prefix . 'IBLOCK_TYPE'], '=ACTIVE' => 'Y')
			));
			$iblocks = array();
			while($item = $iterator->fetch())
			{
				if(strlen($item['CODE']))
				{
					$iblocks[$item['CODE']] = '[' . $item['CODE'] . '] ' . $item['NAME'];
				}
				else
				{
					$iblocks[$item['ID']] = '[' . $item['ID'] . '] ' . $item['NAME'];
				}
			}

			$arComponentParameters['PARAMETERS'][$prefix . 'IBLOCK_ID'] = array(
				'PARENT' => $parent,
				'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_IBLOCK_ID'),
				'TYPE' => 'LIST',
				'ADDITIONAL_VALUES' => 'N',
				'VALUES' => $iblocks,
				'REFRESH' => 'Y',
			);
		}
	}

	/**
	 * @param $arComponentParameters
	 * @param string $prefix
	 * @param string $parent
	 */
	public static function addCatalogSettings(&$arComponentParameters, $prefix = '', $parent = 'PRICES')
	{
		if(Loader::includeModule('catalog'))
		{
			$price = \CCatalogIBlockParameters::getPriceTypesList();

			$arComponentParameters['PARAMETERS'][$prefix . 'PRICE_CODE'] = array(
				'PARENT' => $parent,
				'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_PRICE_CODE'),
				'TYPE' => 'LIST',
				'VALUES' => $price,
				'MULTIPLE' => 'Y',
			);
		}
	}

	/**
	 * @param $arComponentParameters
	 * @param string $prefix
	 * @param string $parent
	 */
	public static function addElementsSettings(&$arComponentParameters, $prefix = '', $parent = 'DATA_SOURCE')
	{
		$order = array(
			'ASC' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_ASC'),
			'DESC' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_DESC'),
		);

		$sort = CIBlockParameters::GetElementSortFields(array(), array('KEY_LOWERCASE' => 'N'));

		$arComponentParameters['PARAMETERS'][$prefix . 'SORT_BY1'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_BY1'),
			'TYPE' => 'LIST',
			'VALUES' => $sort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'SORT',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'SORT_ORDER1'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_ORDER1'),
			'TYPE' => 'LIST',
			'VALUES' => $order,
			'DEFAULT' => 'ASC',
			'ADDITIONAL_VALUES' => 'Y',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'SORT_BY2'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_BY2'),
			'TYPE' => 'LIST',
			'VALUES' => $sort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'ID',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'SORT_ORDER2'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $order,
			'DEFAULT' => 'DESC',
			'ADDITIONAL_VALUES' => 'Y',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'SECTION_ID'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SECTION_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'SECTION_CODE'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_SECTION_CODE'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'INCLUDE_SUBSECTIONS'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS'),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'Y' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_ALL'),
				'A' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_ACTIVE'),
				'N' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_INCLUDE_SUBSECTIONS_NO'),
			),
			'DEFAULT' => 'Y',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'PAGE_ELEMENT_COUNT'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_PAGE_ELEMENT_COUNT'),
			'TYPE' => 'STRING',
			'DEFAULT' => '30',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'CHECK_DATES'] = array(
			'PARENT' => $parent,
			'NAME' => Loc::getMessage('CORE_COMPONENT_PARAMETERS_CHECK_DATES'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
		);

		$arComponentParameters['PARAMETERS'][$prefix . 'ACTIVE_DATE_FORMAT'] = CIBlockParameters::GetDateFormat(Loc::getMessage('CORE_COMPONENT_PARAMETERS_ACTIVE_DATE_FORMAT'), $parent);
		$arComponentParameters['PARAMETERS'][$prefix . 'FIELD_CODE'] = CIBlockParameters::GetFieldCode(Loc::getMessage('CORE_COMPONENT_PARAMETERS_IBLOCK_FIELD'), $parent, array('SECTION_ID'));
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	public static function parseDefaultSettings(array &$params)
	{
		Tools::parseWhiteList($params['CACHE_TYPE'], ['A', 'Y', 'N']);
		Tools::parseInt($params['CACHE_TIME'], 36000000);
		Tools::parseBoolean($params['CACHE_TEMPLATE']);
		Tools::parseStringStrict($params['FILTER_NAME']);
		Tools::parseBoolean($params['SAVE_IN_SESSION']);

		if($params['SAVE_IN_SESSION'] && $params['FILTER_NAME'])
		{
			if(isset($GLOBALS[$params['FILTER_NAME']]))
			{
				$params['FILTER'] = $_SESSION[$params['FILTER_NAME']] = $GLOBALS[$params['FILTER_NAME']];
			}
			else
			{
				$params['FILTER'] = $_SESSION[$params['FILTER_NAME']];
			}
		}
		elseif($params['FILTER_NAME'])
		{
			$params['FILTER'] = $GLOBALS[$params['FILTER_NAME']];
		}

		return $params;
	}

	/**
	 * @param array $params
	 * @param string $prefix
	 *
	 * @return array
	 */
	public static function parseIblockSettings(array &$params, $prefix = '')
	{
		Tools::parseStringStrict($params[$prefix . 'IBLOCK_TYPE']);
		Tools::parseIblockId($params[$prefix . 'IBLOCK_ID'], $params[$prefix . 'IBLOCK_TYPE']);

		return $params;
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	public static function parseTitle(array &$params)
	{
		Tools::parseBoolean($params['SET_TITLE']);

		return $params;
	}

	/**
	 * @param array $params
	 * @param string $prefix
	 *
	 * @return array
	 */
	public static function parseCatalogSettings(array &$params, $prefix = '')
	{
		Tools::parseArrayValuesEmpty($params[$prefix . 'PRICE_CODE']);

		return $params;
	}

	/**
	 * @param array $params
	 * @param string $prefix
	 *
	 * @return array
	 */
	public static function parseElementsSettings(array &$params, $prefix = '')
	{
		Tools::parseStringStrict($params[$prefix . 'SORT_BY1'], 'sort');
		Tools::parseStringCheck($params[$prefix . 'SORT_ORDER1'], 'asc', '/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i');
		Tools::parseStringStrict($params[$prefix . 'SORT_BY2'], 'id');
		Tools::parseStringCheck($params[$prefix . 'SORT_ORDER2'], 'desc', '/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i');
		Tools::parseInt($params[$prefix . 'SECTION_ID']);
		Tools::parseStringStrict($params[$prefix . 'SECTION_CODE']);
		Tools::parseWhiteList($params[$prefix . 'INCLUDE_SUBSECTIONS'], array('Y', 'A', 'N'));
		Tools::parseInt($params[$prefix . 'PAGE_ELEMENT_COUNT']);
		Tools::parseBoolean($params[$prefix . 'CHECK_DATES']);
		Tools::parseString($params[$prefix . 'ACTIVE_DATE_FORMAT']);
		Tools::parseArrayValuesEmpty($params[$prefix . 'FIELD_CODE']);

		return $params;
	}
}