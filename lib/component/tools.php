<?php

namespace Base\Core\Component;

use Bitrix\Iblock\Component\Tools as IblockTools;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main;
use CFile;
use CIBlockFormatProperties;

/**
 * Provides various useful methods.
 *
 * @package Base\Core\Component
 */
class Tools extends IblockTools
{
	public static function processAuth($message = "", $defineConstant = true, $setStatus = true)
	{
		/** @global \CMain $APPLICATION */
		global $APPLICATION;

		if ($defineConstant && !defined("NEED_AUTH"))
		{
			define("NEED_AUTH", true);
		}

		if ($setStatus)
		{
			\CHTTP::setStatus("403 Forbidden");
		}

		if ($APPLICATION->RestartWorkarea())
		{
			$APPLICATION->AuthForm($message);
		}
	}

	/**
	 * Function reduces input value to integer type, and, if gets null, passes the default value
	 *
	 * @param mixed $field Field value
	 * @param bool|int $default Default value
	 * @param bool|int $allowZero Allows zero-value of the parameter
	 *
	 * @return int Parsed value
	 */
	public static function parseInt(&$field, $default = false, $allowZero = false)
	{
		$field = intval($field);
		if(!$allowZero && !$field && $default !== false)
		{
			$field = $default;
		}

		return $field;
	}

	/**
	 * Function get Iblock id
	 *
	 * @param mixed $id Id or code iblock
	 * @param string $type Iblock type
	 *
	 * @return int
	 */
	public static function parseIblockId(&$id, $type)
	{
		if(!is_numeric(substr($id, 0, 1)) && Main\Loader::includeModule('iblock'))
		{
			$managedCache = Main\Application::getInstance()->getManagedCache();

			$uniqueId = md5($id . $type);

			if($managedCache->read(CACHED_b_iblock, $uniqueId, 'b_iblock'))
			{
				$iblock = $managedCache->get($uniqueId);
			}
			else
			{
				$iblock = IblockTable::getList(array(
					'filter' => array('=CODE' => $id, '=IBLOCK_TYPE_ID' => $type, '=ACTIVE' => 'Y'),
					'select' => array('ID', 'CODE', 'IBLOCK_TYPE_ID')
				))->fetch();

				$managedCache->set($uniqueId, $iblock);
			}

			$id = $iblock['ID'];
		}

		return $id;
	}

	/**
	 * Function forces 'Y'/'N' value to boolean
	 *
	 * @param mixed $field Field value
	 * @param string $default Default value
	 *
	 * @return string parsed value
	 */
	public static function parseBoolean(&$field, $default = 'Y')
	{
		$field = $field == $default;

		return $field;
	}

	/**
	 * Function processes string value and, if gets null, passes the default value to it
	 *
	 * @param mixed $field Field value
	 * @param string $default Default value
	 *
	 * @return string parsed value
	 */
	public static function parseString(&$field, $default = '')
	{
		$field = trim((string)$field);
		if(!strlen($field) && $default !== false)
		{
			$field = $default;
		}

		$field = htmlspecialcharsbx($field);

		return $field;
	}

	/**
	 * Function processes regular expression match string value
	 *
	 * @param mixed $field Field value
	 * @param string $default Default value
	 * @param string $pattern The pattern to search for, as a string
	 *
	 * @return string parsed value
	 */
	public static function parseStringCheck(&$field, $default, $pattern = '#[^a-z0-9_-]#i')
	{
		$field = trim((string)$field);

		if(!preg_match($pattern, $field))
		{
			$field = $default;
		}

		return $field;
	}

	/**
	 * Function processes parameter value by white list, if gets null, passes the first value in white list
	 *
	 * @param mixed $field Field value
	 * @param array $list List value
	 *
	 * @return string parsed value
	 */
	public static function parseWhiteList(&$field, $list)
	{
		if(!in_array($field, $list))
		{
			$field = current($list);
		}

		return $field;
	}

	public static function parseArrayValuesEmpty(&$field)
	{
		foreach((array)$field as $key => $value)
		{
			if(empty($value))
			{
				unset($field[$key]);
			}
		}

		return $field;
	}

	/**
	 * Function checks if it`s argument is a legal array for foreach() construction
	 *
	 * @param mixed $array Data to check
	 *
	 * @return boolean
	 */
	public static function isNonEmptyArray($array)
	{
		return !empty($array) && is_array($array);
	}

	/**
	 * Function return short uri
	 *
	 * @param string $uri
	 * @param string $code
	 *
	 * @return string
	 */
	public static function getShortUri($uri, $code = '')
	{
		$uriCrc32 = \CBXShortUri::Crc32($uri);

		$iterator = \CBXShortUri::GetList(array(), array('URI_CRC' => $uriCrc32));
		while($result = $iterator->Fetch())
		{
			if($result['URI'] == $uri)
			{
				return SITE_DIR . $result['SHORT_URI'];
			}
		}

		if(empty($code))
		{
			$code = \CBXShortUri::GenerateShortUri();
		}
		else
		{
			$code = '~' . static::parseStringStrict($code);
			$iterator = \CBXShortUri::GetList(array(), array('SHORT_URI' => $code));
			$result = $iterator->Fetch();
			if($result['ID'])
			{
				\CBXShortUri::Delete($result['ID']);
			}
		}

		$fields = array(
			'URI' => $uri,
			'SHORT_URI' => $code,
			'STATUS' => 301,
		);

		if(\CBXShortUri::Add($fields))
		{
			return SITE_DIR . $fields['SHORT_URI'];
		}
		else
		{
			return $uri;
		}
	}

	/**
	 * Function processes string value and, if gets null, passes the default value to it
	 *
	 * @param mixed $field Field value
	 * @param bool|string $default Default value
	 * @param mixed $pattern The pattern to search for. It can be either a string or an array with strings
	 *
	 * @return string parsed value
	 */
	public static function parseStringStrict(&$field, $default = false, $pattern = '#[^a-z0-9_-]#i')
	{
		$field = trim((string)$field);
		if(!strlen($field) && $default !== false)
		{
			$field = $default;
		}

		$field = preg_replace($pattern, '', $field);

		return $field;
	}

	/**
	 * Function parse phone to format 79999999999
	 *
	 * @param string $field Phone
	 *
	 * @return int|bool Parsed value
	 */
	public static function parsePhone(&$field)
	{
		$field = static::parseStringStrict($field, false, '/[^\d]/' . BX_UTF_PCRE_MODIFIER);

		if(strlen($field) != 11)
		{
			return false;
		}

		if($field[0] != 7)
		{
			$field[0] = 7;
		}

		return $field;
	}

	/**
	 * Function get prepared for a date
	 *
	 * @param string $date
	 * @param string $format
	 *
	 * @return string
	 */
	public static function getDisplayDate($date, $format = 'j F Y')
	{
		if(strlen($format) > 0 && strlen($date) > 0)
		{
			return CIBlockFormatProperties::DateFormat($format, strtotime($date));
		}

		return $date;
	}

	/**
	 * Function prepares picture or resize
	 *
	 * @param int $id
	 * @param bool $width
	 * @param bool $height
	 * @param int $type
	 * @param int $ratio
	 *
	 * @return array|bool
	 */
	public static function resizePicture($id, $width = false, $height = false, $type = BX_RESIZE_IMAGE_PROPORTIONAL, $ratio = 2)
	{
		$file = CFile::GetFileArray($id);

		if($width && $height && (is_array($file) && array_key_exists('FILE_NAME', $file) && strlen($file['FILE_NAME']) > 0))
		{
			$file['RESIZE'] = CFile::ResizeImageGet($file, array('width' => $width, 'height' => $height), $type, true);
			$file['RESIZE'] = array_change_key_case($file['RESIZE'], CASE_UPPER);

			if($ratio > 1 && $file['WIDTH'] > $width * $ratio && $file['HEIGHT'] > $height * $ratio)
			{
				$file['RETINA'] = CFile::ResizeImageGet($file, array('width' => $width * $ratio, 'height' => $height * $ratio), $type, true);
				$file['RETINA'] = array_change_key_case($file['RETINA'], CASE_UPPER);
				$file['SRCSET'] = ' srcset="' . $file['RETINA']['SRC'] . ' ' . $ratio . 'x" ';
			}
		}

		return $file;
	}

	/**
	 * Get numeric case for lang messages.
	 *
	 * @param $number
	 * @param $once
	 * @param $multi21
	 * @param $multi2_4
	 * @param $multi5_20
	 *
	 * @return string
	 */
	public static function getNumericCase($number, $once, $multi21, $multi2_4, $multi5_20)
	{
		if($number == 1)
		{
			return $once;
		}

		if($number < 0)
		{
			$number = -$number;
		}

		$number %= 100;
		if($number >= 5 && $number <= 20)
		{
			return $multi5_20;
		}

		$number %= 10;
		if($number == 1)
		{
			return $multi21;
		}

		if($number >= 2 && $number <= 4)
		{
			return $multi2_4;
		}

		return $multi5_20;
	}
}