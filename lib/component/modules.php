<?php

namespace Base\Core\Component;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;

trait Modules
{
	/**
	 * Function checks if required modules installed. If not, throws an exception.
	 *
	 * @see listModules().
	 * @throws LoaderException
	 */
	final protected function checkRequiredModules()
	{
		$modules = $this->listModules();

		if(Tools::isNonEmptyArray($modules))
		{
			foreach($modules as $module)
			{
				if(!Loader::includeModule($module))
				{
					throw new LoaderException(Loc::getMessage('CORE_COMPONENT_ERROR_MODULE_NOT_INSTALLED'), Base::ERROR_MODULE_NOT_INSTALLED);
				}
			}
		}
	}

	/**
	 * Lists all modules required for component.
	 * Simple example:
	 * <code>
	 *    return array('iblock', 'sale');
	 * </code>
	 *
	 * @return array
	 */
	protected function listModules()
	{
		return array();
	}
}