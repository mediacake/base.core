<?php

namespace Base\Core\Component;

use Bitrix\Main\HttpRequest;

trait Controller
{
	/**
	 * Runs action.
	 * Will be run method with name processAction{$actionName}
	 *
	 * @return mixed
	 */
	abstract protected function runAction();

	/**
	 * Gets current action.
	 *
	 * @return string
	 */
	abstract protected function getAction();

	/**
	 * Gets description of action.
	 *
	 * @return array
	 */
	abstract protected function getActionDescription();

	/**
	 * Checks action by settings in description.
	 * This method may terminate controller and application.
	 *
	 * @return void
	 */
	abstract protected function checkAction();

	/**
	 * Resolves action and description of action, which need to run.
	 *
	 * @see listActions().
	 * @return $this
	 */
	abstract protected function resolveAction();

	/**
	 * @param string $action
	 * @param array $description
	 *
	 * @return $this
	 */
	abstract protected function setAction($action, array $description);

	/**
	 * Checks required parameters.
	 * Fills error collection if required parameter is missed.
	 *
	 * @param array $inputParams Input data.
	 * @param array $required Required parameters.
	 *
	 * @return bool
	 */
	abstract protected function checkRequiredInputParams(array $inputParams, array $required);

	/**
	 * Lists all actions by controller. This listing may contains description in short-style.
	 * If you set
	 * array(
	 *  'showTest'
	 * )
	 * Then action 'showTest' has default description @see normalizeActionDescription()
	 * and execute method processActionShowTest.
	 * If you set
	 * array(
	 *  'showFoo' => 'showBar'
	 * )
	 * Then action 'showFoo' has default description @see normalizeActionDescription()
	 * and execute method processActionShowBar.
	 * If you set
	 * array(
	 *  'showFoo' => array(
	 *        'method' => array('GET', 'POST'), //allowed GET and POST methods to run action.
	 *        'name' => 'showFoo', //execute method processActionShowFoo
	 *        'check_csrf_token' => true, // check csrf token and run @see runProcessingIfInvalidCsrfToken()
	 *        'redirect_on_auth' => true, // if user doesn't authorize then will redirect on login page.
	 *  )
	 * )
	 *
	 * @return array
	 */
	protected function listActions()
	{
		return array();
	}

	/**
	 * Normalizes list of action to general view.
	 *
	 * @param array $listOfActions List of action (@see listActions()).
	 *
	 * @return array
	 */
	protected function normalizeListOfAction(array $listOfActions)
	{
		$normalized = array();
		foreach($listOfActions as $action => $description)
		{
			if(!is_string($action))
			{
				$normalized[$description] = $this->normalizeActionDescription($description, $description);
			}
			else
			{
				$normalized[$action] = $this->normalizeActionDescription($action, $description);
			}
		}
		unset($action, $description);

		return array_change_key_case($normalized, CASE_LOWER);
	}

	/**
	 * Normalizes action description.
	 * Default description:
	 * array(
	 *            'method' => array('GET'), //allowed methods to run action.
	 *            'name' => $action, //action which will run
	 *            'check_csrf_token' => false, // check csrf token
	 *            'redirect_on_auth' => true, // if user doesn't authorize then will redirect on login page.
	 *    )
	 *
	 * @param string $action Action name.
	 * @param array|string $description Action description.
	 *
	 * @return array
	 */
	protected function normalizeActionDescription($action, $description)
	{
		if(!is_array($description))
		{
			$description = array(
				'method' => array('GET'),
				'name' => $description,
				'check_csrf_token' => false,
				'redirect_on_auth' => true,
			);
		}
		if(empty($description['name']))
		{
			$description['name'] = $action;
		}
		if(!isset($description['redirect_on_auth']))
		{
			$description['redirect_on_auth'] = false;
		}

		return $description;
	}

	/**
	 * Checks required parameters in $_GET.
	 * Fills error collection if required parameter is missed.
	 *
	 * @param array $required Required parameters.
	 * @param HttpRequest $request
	 *
	 * @return bool
	 */
	protected function checkRequiredGetParams(array $required, HttpRequest $request)
	{
		$params = array();
		foreach($required as $item)
		{
			$params[$item] = $request->getQuery($item);
		}
		unset($item);

		return $this->checkRequiredInputParams($params, $required);
	}

	/**
	 * Checks required parameters in $_POST.
	 * Fills error collection if required parameter is missed.
	 *
	 * @param array $required Required parameters.
	 * @param HttpRequest $request
	 *
	 * @return bool
	 */
	protected function checkRequiredPostParams(array $required, HttpRequest $request)
	{
		$params = array();
		foreach($required as $item)
		{
			$params[$item] = $request->getPost($item);
		}
		unset($item);

		return $this->checkRequiredInputParams($params, $required);
	}

	/**
	 * Checks required parameters in $_FILES.
	 * Fills error collection if required parameter is missed.
	 *
	 * @param array $required Required parameters.
	 * @param HttpRequest $request
	 *
	 * @return bool
	 */
	protected function checkRequiredFilesParams(array $required, HttpRequest $request)
	{
		$params = array();
		foreach($required as $item)
		{
			$params[$item] = $request->getFile($item);
		}
		unset($item);

		return $this->checkRequiredInputParams($params, $required);
	}

	/**
	 * Common operations before process action.
	 *
	 * @param string $actionName Action name which will be run.
	 *
	 * @return bool If method will return false, then action will not execute.
	 */
	protected function processBeforeAction($actionName)
	{
		return true;
	}
}