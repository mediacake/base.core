<?php

namespace Base\Core\Component;

use Base\Core\Globals;
use Bitrix\Main\Loader;
use CBitrixComponent;
use CComponentEngine;

/**
 * @package Base\Core\Component
 */
class Router extends CBitrixComponent
{
	use Globals;

	/**
	 * Function implements all the life cycle of our component
	 *
	 * @return mixed value
	 */
	public function executeComponent()
	{
		$defaultUrlTemplates404 = array();
		$defaultVariableAliases404 = array();
		$componentVariables = array();

		$variables = array();

		$engine = new CComponentEngine($this);
		if(Loader::includeModule('iblock'))
		{
			$engine->addGreedyPart("#SECTION_CODE_PATH#");
			$engine->addGreedyPart("#SMART_FILTER_PATH#");
			$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
		}

		$urlTemplates = CComponentEngine::MakeComponentUrlTemplates($defaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
		$variableAliases = CComponentEngine::MakeComponentVariableAliases($defaultVariableAliases404, $this->arParams["VARIABLE_ALIASES"]);

		$componentPage = $engine->guessComponentPath(
			$this->arParams["SEF_FOLDER"],
			$urlTemplates,
			$variables
		);

		if($componentPage)
		{
			CComponentEngine::InitComponentVariables($componentPage, $componentVariables, $variableAliases, $variables);

			$this->arResult = array(
				"FOLDER" => $this->arParams["SEF_FOLDER"],
				"URL_TEMPLATES" => $urlTemplates,
				"VARIABLES" => $variables,
				"ALIASES" => $variableAliases
			);

			$this->IncludeComponentTemplate($componentPage);
		}
		else
		{
			Tools::process404('', true, true, true);
		}

		return true;
	}
}